function TestApp () {
  this.arr = [];
  this.buttonBig = document.querySelector('.buttonTestPlain');
  this.buttonHard = document.querySelector('.buttonTestHard');
  this.wrapper = document.querySelector('.wrapperPlain');
}

TestApp.prototype.addObj = function () {
  this.wrapper.innerHTML = '';
  var newObj = this.createObj();
  newObj.id = this.arr.length + 1;
  newObj.text = 'Простой блок: ' + Math.random().toString(36).substring(7);
  this.arr.push(newObj);
  this.showProducts();
};

TestApp.prototype.addObjHard = function () {
  this.wrapper.innerHTML = '';
  let newObj = this.createObj()
  newObj.id = this.arr.length + 1;
  newObj.hard = true;
  newObj.text = 'Сложный блок: ' + Math.random().toString(36).substring(7);
  this.arr.push(newObj);
  this.showProducts();
};

TestApp.prototype.createObj = function () {
  return {}
};

TestApp.prototype.showProducts = function (){
  this.arr.forEach(function (element) {
    var div = document.createElement('div');
    this.wrapper.appendChild(div);
    div.className = 'textDiv';

    var p = document.createElement('p');
    div.appendChild(p);
    p.innerHTML = element.text;
    p.className = 'p_new';

    var button = document.createElement('button');
    div.insertBefore(button, p);
    button.className = 'myButton';

    if(!element.hard){
      button.onclick = this.deleteItems(element.id).bind(this);
    }

    var icon = document.createElement('i');
    button.appendChild(icon);
    icon.className = 'fas fa-times';

    if(!element.cheked){
      div.addEventListener("click", function(){
        this.chekedItems(element, div)
      }.bind(this));
    };

    if(element.hard){
      div.addEventListener("dblclick", function(){
        this.addState(element, div)
        this.checkAllBlockHard()
        this.checkBlocksRed()
        this.checkBlocksGreed()
      }.bind(this));
      button.addEventListener("click", function(){
        this.openModal(element)
      }.bind(this));
    }
  }.bind(this));
  this.checkAllBlock();
  this.checkAllBlockHard();
  this.checkBlocksRed();
  this.checkBlocksGreed ();
};

TestApp.prototype.checkAllBlock = function () {
  var allBlocks = this.arr.length;
  var allBlocksInfo = document.querySelector('.allBlocksInfo');
  console.log(allBlocks)
  allBlocksInfo.innerHTML = `${allBlocks}`;
}

TestApp.prototype.checkAllBlockHard = function () {
  var chekedBlocksInfo = document.querySelector('.chekedBlocksInfo');
  var chekedBlocks = this.arr.filter(element => element.state || element.cheked);
  console.log(chekedBlocks)
  chekedBlocksInfo.innerHTML = `${chekedBlocks.length}`;
}

TestApp.prototype.checkBlocksRed = function () {
  var chekedBlocksInfoRed = document.querySelector('.chekedBlocksInfoRed');
  var chekedBlocksRed = this.arr.filter(element => element.state === 'red');
  console.log(chekedBlocksRed)
  chekedBlocksInfoRed.innerHTML = `${chekedBlocksRed.length}`;
}

TestApp.prototype.checkBlocksGreed = function () {
  var chekedBlocksInfoGreen = document.querySelector('.chekedBlocksInfoGreen');
  var chekedBlocksGreen = this.arr.filter(element => element.state === 'green');
  console.log(chekedBlocksGreen)
  chekedBlocksInfoGreen.innerHTML = `${chekedBlocksGreen.length}`;
}

TestApp.prototype.chekedItems = function (element, div){
  element.cheked = true;
  div.classList.add('clicked');
};

TestApp.prototype.openModal = function (element) {
  var modal = document.querySelector('.modal');
  modal.classList.add('show')
  let btnClose = document.querySelector('.btn_close');
  btnClose.addEventListener("click", function(){
    modal.classList.remove('show');
  });
  let btnDelete = document.querySelector('.btn_delete');
  btnDelete.onclick = this.deleteHardItems(element.id, modal).bind(this);
};

TestApp.prototype.deleteHardItems = function (id, modal) {
  var elementId = id;
  return function () {
    for (var e = 0; e < this.arr.length; e++){
      if(this.arr[e].id === elementId){
        this.arr.splice(e, 1);
        modal.classList.remove('show');
        this.wrapper.innerHTML = "";
        this.showProducts();
      }
    }
  }
};

TestApp.prototype.deleteItems = function (id) {
  var elementId = id;
  return function () {
    for(var x = 0; x < this.arr.length; x++){
      if(this.arr[x].id === elementId){
        this.arr.splice(x, 1);
        this.wrapper.innerHTML = "";
        this.showProducts();
      }
    }
  }
};

TestApp.prototype.addState = function (element, div){
  if(!element.state){
    element.state = 'green';
    div.className = 'green';
  } else if (element.state === 'green') {
    element.state = 'red';
    div.className = 'red';
  } else {
    element.state = undefined;
    div.className = 'textDiv'
  }
};


TestApp.prototype.init = function () {
  this.buttonBig.addEventListener("click", this.addObj.bind(this));
  this.buttonHard.addEventListener("click", this.addObjHard.bind(this));
}

var Application = new TestApp()

Application.init()
